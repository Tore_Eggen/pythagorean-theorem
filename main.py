import pygame
import math


modes = open("modes", "r")
print(modes.read())
'''mode checker'''
mode = input("Mode: ")

if mode == "1":
    kat1 = round(float(input("\n Katet 1: ")), 2)
    kat2 = round(float(input("\n Katet 2: ")), 2)

    hyp = round(math.sqrt((kat1 ** 2) + (kat2 ** 2)), 2)

    print(f"\nHypotenusen er {hyp} lang\n")

elif mode == "2":
    kat1 = round(float(input("Katet 1: \n")), 2)

    hyp = kat1 * 2

    kat2 = round(math.sqrt((hyp ** 2) - (kat1) ** 2), 2)

    print(f"Den lengste kateten er {kat2} lang\nHypotenusen er {hyp} lang")


elif mode == "3":
    hyp = round(float(input("Hypotenus: \n")), 2)
    kat1 = round(hyp / 2, 2)

    kat2 = round(math.sqrt((hyp ** 2) - (kat1) ** 2), 2)

    print(f"Den lengste kateten er {kat2} lang\nDen korteste kateten er {kat1} lang")

elif mode == "4":
    kat2 = round(float(input("\nLengde på lengste katet: ")), 2)

    kat1 = round(math.sqrt(kat2 ** 2 / 3), 2)
    hyp = round(kat1 * 2, 2)

    print(f"Den korteste kateten er {kat1} lang \nHypotenusen er {hyp} lang")

pygame.init()
pygame.font.init()

white = (255, 255, 255)
black = (0, 0, 0)
red = (255, 0, 0)
blue = (0, 255, 0)

font=pygame.font.SysFont('Comic Sans MS', 30)
kat1txt=font.render(f'kat1 = {kat1}', False, black)
kat2txt=font.render(f'kat2 = {kat2}', False, black)
hyptxt=font.render(f'hyp = {hyp}', False, black)

a=[300, 100]
b=[300, 100+(kat2*50)]
c=[300+(kat1*50), 100+(kat2*50)]

size=[1920, 1020]

screen = pygame.display.set_mode(size)

pygame.display.set_caption("pytagaros")
pygame.display.set_icon(pygame.image.load('janern.png'))


done=False
clock = pygame.time.Clock()

while not done:

    clock.tick(10)

    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            done=True

    screen.fill(white)
    pygame.draw.line(screen, black, a, b, 5)
    pygame.draw.line(screen, red, b, c, 5)
    pygame.draw.line(screen, blue, c, a, 5)
    screen.blit(kat1txt, (100, 100))
    screen.blit(kat2txt, (100, 200))
    screen.blit(hyptxt, (100, 300))

    pygame.display.flip()
